package com.github.zuihou.authority.service.core;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.zuihou.authority.entity.core.Station;

/**
 * <p>
 * 业务接口
 * 
 * </p>
 *
 * @author zuihou
 * @date 2019-07-03
 */
public interface StationService extends IService<Station> {

}
