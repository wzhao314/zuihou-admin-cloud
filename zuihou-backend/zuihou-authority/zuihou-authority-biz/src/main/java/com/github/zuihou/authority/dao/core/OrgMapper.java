package com.github.zuihou.authority.dao.core;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.zuihou.authority.entity.core.Org;

import org.springframework.stereotype.Repository;

/**
 * <p>
 * Mapper 接口
 * 
 * </p>
 *
 * @author zuihou
 * @date 2019-07-03
 */
@Repository
public interface OrgMapper extends BaseMapper<Org> {

}
